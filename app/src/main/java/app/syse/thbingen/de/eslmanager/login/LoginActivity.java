package app.syse.thbingen.de.eslmanager.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import java.io.IOException;

import app.syse.thbingen.de.eslmanager.MainActivity;
import app.syse.thbingen.de.eslmanager.R;
import app.syse.thbingen.de.eslmanager.intro.IntroActivity;
import app.syse.thbingen.de.eslmanager.network.SingletonCookieManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

//import java.net.CookiePolicy;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private String BASE_URL;
    public final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public String session_token = "empty";
    public String session_id = "empty";
    Context c;

    SingletonCookieManager scm;
    OkHttpClient client;

    private SharedPreferences prefs;

    @BindView(R.id.input_username)
    EditText _usernameText;
    @BindView(R.id.input_password)
    EditText _passwordText;
    @BindView(R.id.input_ip)
    EditText _ipText;
    @BindView(R.id.btn_login)
    Button _loginButton;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        c = this;

        prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        BASE_URL = prefs.getString("BASE_URL", "192.168.178.178");
        _ipText.setText(BASE_URL);

        scm = SingletonCookieManager.getInstance();

        client = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(scm.getCookieManager()))
                .build();

        showIntro();


        final AlertDialog alertDialog = new AlertDialog.Builder(c).create();
        alertDialog.setTitle("Hinweis zur Benutzung");
        alertDialog.setMessage("Diese App ist ein Prototyp und befindet sich in der Alpha-Phase.\nAbstürze und Fehler sind möglich und werden mit zukünftigen Versionen gefixt.");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Akzeptieren",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        alertDialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Ablehnen",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alertDialog.show();



        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("BASE_URL", _ipText.getText().toString());
                editor.commit();

                login();
            }
        });

    }

    public void login(){
        String username = _usernameText.getText().toString();
        String password = _passwordText.getText().toString();
        this.login(username, password);
    }

    public void login(String username, String password) {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        prefs.edit().putString("Username", username).commit();
        _loginButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);

        // TODO: Implement your own authentication logic here.
        AT_Login at_login = new AT_Login();
        at_login.execute(buildRequestLogin(username, password));
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);

        Intent myIntent = new Intent(this, MainActivity.class);
        progressBar.setVisibility(View.INVISIBLE);
        this.startActivity(myIntent);
    }

    public void onLoginFailed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.INVISIBLE);
                _loginButton.setEnabled(true);
            }
        });

    }

    public boolean validate() {
        boolean valid = true;

        String username = _usernameText.getText().toString();
        String password = _passwordText.getText().toString();

        if (username.isEmpty()) {
            _usernameText.setError("Bitte einen Namen eingeben");
            valid = false;
        } else {
            _usernameText.setError(null);
        }

        if (password.isEmpty()) {
            _passwordText.setError("Bitte ein Passwort eingeben");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private class AT_Login extends AsyncTask<Request, Void, Void> {
        @Override
        protected Void doInBackground(Request... params) {
            sendRequest(params[0]);
            return null;
        }
    }

    private void sendRequest(final Request request) {

        CookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(c));


        //OkHttpClient client = new OkHttpClient();
        final String errMessage = "Login fehlgeschlagen";
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("*** Failure ***", "Code: " + e.getMessage());
                //new NetworkErrDialogCreator(c).showNoResponseDialog(errMessage);
                onLoginFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("*** Response Code ***", "Code: " + response.code());
                Log.d("*** Response ***", response.body().string());

                if (response.code() == 200){
                    checkLogin();
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onLoginFailed();
                        }
                    });
                }

            }
        });
    }

    private void checkLogin() {


        //OkHttpClient client = new OkHttpClient();
        final String errMessage = "Login fehlgeschlagen";
        client.newCall(buildRequestCheckLogin()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("*** Failure ***", "Code: " + e.getMessage());
                //new NetworkErrDialogCreator(c).showNoResponseDialog(errMessage);
                onLoginFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("*** Response Code ***", "Code: " + response.code());
                boolean success_tmp = false;
                if (response.code() == 200){
                    String responseBody = response.body().string();
                    Log.d("Logged in", responseBody);
                    if (responseBody.equals("true")) {
                        success_tmp = true;
                    } else {
                        Log.d("Logged in", responseBody + " != true");
                    }
                }

                final boolean success = success_tmp;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (success) {
                            onLoginSuccess();
                        } else {
                            onLoginFailed();
                        }
                    }
                });

            }
        });
    }


    public Request buildRequestLogin(String username, String password){
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host(BASE_URL)
                .port(8080)
                .addPathSegment("login")
                .addQueryParameter("username", username)
                .addQueryParameter("password", password)
                .build();

        String url = httpUrl.toString();
        Log.d("URl", url);

        RequestBody reqbody = RequestBody.create(null, new byte[0]);

        Request request = new Request.Builder()
                .url(url)
                .method("POST", reqbody)
                .build();
        Log.d("Request", request.method());

        return request;
    }

    public Request buildRequestCheckLogin(){
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host(BASE_URL)
                .port(8080)
                .addPathSegments("security/loggedIn")
                .build();

        String url = httpUrl.toString();
        Log.d("URl", url);

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Log.d("Request", request.method());

        return request;
    }

    private void showIntro(){
        //prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        Boolean firstStart = prefs.getBoolean("firstStart", true);
        Log.d("***** First start *****", firstStart+"");
        if (firstStart) {
            Intent intent = new Intent(this, IntroActivity.class);
            startActivity(intent);
        }
    }

//ToDo Login Dtaen speichern bei Wunsch, Einstekllungen

}