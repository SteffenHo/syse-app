package app.syse.thbingen.de.eslmanager.entities;

/**
 * Created by Steffen on 25.12.2017.
 */

public class Product {

    private int id;
    String name;
    float price;
    String amount;
    String sku;

    /*public Product(int id, String name, float price, String amount, String sku) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.sku = sku;
    }*/

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", amount='" + amount + '\'' +
                ", sku='" + sku + '\'' +
                '}';
    }
}
