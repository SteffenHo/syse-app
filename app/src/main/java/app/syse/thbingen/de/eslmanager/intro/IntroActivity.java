package app.syse.thbingen.de.eslmanager.intro;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import app.syse.thbingen.de.eslmanager.R;

/**
 * Created by Steffen on 28.08.2017.
 */

public class IntroActivity extends AppIntro{

    //ToDo Intro anpassen
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance("We need Camera permission.", "We want to scan some codes", R.drawable.ic_skip_white, getColor(R.color.colorAccent)));
        askForPermissions(new String[]{Manifest.permission.CAMERA}, 1);
        //askForPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        //askForPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        addSlide(AppIntroFragment.newInstance("Thanks", "Have fun", R.drawable.ic_skip_white, getColor(R.color.colorAccent)));

        showSkipButton(false);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        SharedPreferences prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        prefs.edit().putBoolean("firstStart", false).commit();
        finish();
    }
}
