package app.syse.thbingen.de.eslmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import app.syse.thbingen.de.eslmanager.network.SingletonCookieManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EditLabelActiviy extends AppCompatActivity {

    SingletonCookieManager scm;
    OkHttpClient client;
    private String BASE_URL;
    Context c;

    String labelId, name, amount, sku;
    int productId, eslId;
    float price;

    @BindView(R.id.labelId)
    TextView tv_labelId;

    @BindView(R.id.detail_product_id)
    EditText et_productId;

    @BindView(R.id.detail_name)
    EditText et_Name;

    @BindView(R.id.detail_price)
    EditText et_Price;

    @BindView(R.id.detail_amount)
    EditText et_Amount;

    @BindView(R.id.detail_sku)
    EditText et_Sku;

    @BindView(R.id.btn_create)
    Button btn;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_label_activiy);
        ButterKnife.bind(this);

        scm = SingletonCookieManager.getInstance();
        c = this;

        SharedPreferences sharedpreferences = getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        BASE_URL = sharedpreferences.getString("BASE_URL", "192.168.178.178");

        client = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(scm.getCookieManager()))
                .build();


        Intent i = this.getIntent();
        eslId = i.getIntExtra("eslId", -1);
        labelId = i.getStringExtra("labelId");
        productId = i.getIntExtra("productId", -1);

        Log.d("Info", labelId + ", " + productId);

        tv_labelId.setText(labelId);

        et_productId.setText(productId + "");
        et_productId.setEnabled(false);

        if (productId != -1) {
            name = i.getStringExtra("name");
            et_Name.setText(name);

            price = i.getFloatExtra("price", 0.00F);
            et_Price.setText("" + price);

            amount = i.getStringExtra("amount");
            et_Amount.setText(amount);

            sku = i.getStringExtra("sku");
            et_Sku.setText(sku);
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()){
                    progressBar.setVisibility(View.VISIBLE);
                    AT_CheckProduct at_checkProduct = new AT_CheckProduct();
                    at_checkProduct.execute(buildCheckProductRequest());
                }
            }
        });

    }

    private boolean validate() {
        boolean valid = true;

        String name = et_Name.getText().toString();
        String price = et_Price.getText().toString();
        String amount = et_Amount.getText().toString();
        String sku = et_Sku.getText().toString();

        if (name.isEmpty()) {
            et_Name.setError("Bitte einen Namen eingeben");
            valid = false;
        } else {
            et_Name.setError(null);
        }

        if (price.isEmpty()) {
            et_Price.setError("Bitte einen Preis eingeben");
            valid = false;
        } else {
            et_Price.setError(null);
        }

        if (amount.isEmpty()) {
            et_Amount.setError("Bitte eine Menge eingeben");
            valid = false;
        } else {
            et_Amount.setError(null);
        }

        if (sku.isEmpty()) {
            et_Sku.setError("Bitte einen SKU eingeben");
            valid = false;
        } else {
            et_Sku.setError(null);
        }

        return valid;
    }

    private Request buildCheckProductRequest() {
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host(BASE_URL)
                .port(8080)
                .addPathSegment("products")
                .addPathSegment("checkProduct")
                .addQueryParameter("name", et_Name.getText().toString())
                .addQueryParameter("price", et_Price.getText().toString())
                .addQueryParameter("amount", et_Amount.getText().toString())
                .addQueryParameter("sku", et_Sku.getText().toString())
                .build();

        String url = httpUrl.toString();
        Log.d("URl", url);

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        return request;
    }

    private Request buildCreateProductRequest() {
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host(BASE_URL)
                .port(8080)
                .addPathSegment("products")
                .addQueryParameter("name", et_Name.getText().toString())
                .addQueryParameter("price", et_Price.getText().toString())
                .addQueryParameter("amount", et_Amount.getText().toString())
                .addQueryParameter("sku", et_Sku.getText().toString())
                .build();

        String url = httpUrl.toString();
        Log.d("URl", url);

        RequestBody reqbody = RequestBody.create(null, new byte[0]);

        Request request = new Request.Builder()
                .url(url)
                .post(reqbody)
                .build();
        return request;
    }

    private Request buildPatchProductRequest(int prodId) {
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host(BASE_URL)
                .port(8080)
                .addPathSegment("products")
                .addPathSegment(prodId+"")
                .addQueryParameter("name", et_Name.getText().toString())
                .addQueryParameter("price", et_Price.getText().toString())
                .addQueryParameter("amount", et_Amount.getText().toString())
                .addQueryParameter("sku", et_Sku.getText().toString())
                .build();

        String url = httpUrl.toString();
        Log.d("URl", url);

        RequestBody reqbody = RequestBody.create(null, new byte[0]);

        Request request = new Request.Builder()
                .url(url)
                .patch(reqbody)
                .build();
        return request;
    }

    private Request buildPatchEslRequest(int prodId) {
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host(BASE_URL)
                .port(8080)
                .addPathSegment("esls")
                .addPathSegment(eslId+"")
                .addQueryParameter("productId", prodId+"")
                .addQueryParameter("labelSKU", "")
                .build();

        String url = httpUrl.toString();
        Log.d("URl", url);

        RequestBody reqbody = RequestBody.create(null, new byte[0]);

        Request request = new Request.Builder()
                .url(url)
                .patch(reqbody)
                .build();
        return request;
    }

    private class AT_CheckProduct extends AsyncTask<Request, Void, Void> {
        @Override
        protected Void doInBackground(Request... params) {
            sendCheckProductRequest(params[0]);
            return null;
        }
    }

    private void sendCheckProductRequest(Request request) {
        final String errMessage = "Login fehlgeschlagen";
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("*** Failure ***", "Code: " + e.getMessage());
                //onLoginFailed();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseBody = response.body().string();
                Log.d("*** Response Code ***", "Code: " + response.code());
                Log.d("*** Response ***", responseBody);

                if (response.code() == 200){
                    if (responseBody.equals("-1")){
                        if (productId == -1) {
                            Toast.makeText(c, "Erstelle neues Produkt", Toast.LENGTH_SHORT).show();
                            AT_CreateProduct at_product = new AT_CreateProduct();
                            at_product.execute(buildCreateProductRequest());
                        } else {
                            Toast.makeText(c, "Existierendes Produkt wird geupdated", Toast.LENGTH_SHORT).show();
                            AT_PatchProduct at_product = new AT_PatchProduct();
                            at_product.execute(buildPatchProductRequest(productId));
                        }


                    } else {
                        Toast.makeText(c, "Produkt existiert bereits, verknüpfe ..", Toast.LENGTH_SHORT).show();
                        AT_PatchESL at_esl = new AT_PatchESL();
                        at_esl.execute(buildPatchEslRequest(Integer.parseInt(responseBody)));
                    }

                } else {
                    showResponseError(response);
                }
            }
        });
    }


    private class AT_PatchProduct extends AsyncTask<Request, Void, Void> {
        @Override
        protected Void doInBackground(Request... params) {
            sendPatchProductRequest(params[0]);
            return null;
        }
    }

    private class AT_CreateProduct extends AsyncTask<Request, Void, Void> {
        @Override
        protected Void doInBackground(Request... params) {
            sendCreateProductRequest(params[0]);
            return null;
        }
    }

    private class AT_PatchESL extends AsyncTask<Request, Void, Void> {
        @Override
        protected Void doInBackground(Request... params) {
            sendPatchEslRequest(params[0]);
            return null;
        }
    }

    private void sendCreateProductRequest(Request request) {
        final String errMessage = "Login fehlgeschlagen";
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("*** Failure ***", "Code: " + e.getMessage());
                //onLoginFailed();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseBody = response.body().string();
                Log.d("*** Response Code ***", "Code: " + response.code());
                Log.d("*** Response ***", responseBody);

                if (response.code() == 200){
                    Toast.makeText(c, "Produkt erstellt", Toast.LENGTH_SHORT).show();

                    AT_CheckProduct at_checkProduct = new AT_CheckProduct();
                    at_checkProduct.execute(buildCheckProductRequest());


                } else {
                    showResponseError(response);
                }
            }
        });
    }


    private void sendPatchProductRequest(Request request) {
        final String errMessage = "Patch fehlgeschlagen";
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("*** Failure ***", "Code: " + e.getMessage());
                //onLoginFailed();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseBody = response.body().string();
                Log.d("*** Response Code ***", "Code: " + response.code());
                Log.d("*** Response ***", responseBody);

                if (response.code() == 200){
                    Toast.makeText(c, "Product gepatcht", Toast.LENGTH_SHORT).show();



                    finish();
                } else {
                    showResponseError(response);
                }
            }
        });
    }

    private void sendPatchEslRequest(Request request) {
        final String errMessage = "Patch fehlgeschlagen";
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("*** Failure ***", "Code: " + e.getMessage());
                //onLoginFailed();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseBody = response.body().string();
                Log.d("*** Response Code ***", "Code: " + response.code());
                Log.d("*** Response ***", responseBody);

                if (response.code() == 200) {
                    Toast.makeText(c, "Esl gepatcht", Toast.LENGTH_SHORT).show();


                    finish();
                }else if (response.code() == 404) {
                    Log.w("Wrong Response", "Should be 202");
                    finish();

                } else {
                    showResponseError(response);
                }
            }
        });
    }

    public void showResponseError(final Response response){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                final AlertDialog alertDialog = new AlertDialog.Builder(c).create();
                alertDialog.setTitle("Response " + response.code());
                alertDialog.setMessage(response.message());
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                alertDialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });
    }

}
