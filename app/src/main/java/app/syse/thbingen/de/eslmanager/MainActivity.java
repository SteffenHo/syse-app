package app.syse.thbingen.de.eslmanager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import app.syse.thbingen.de.eslmanager.entities.Esl;
import app.syse.thbingen.de.eslmanager.entities.Product;
import app.syse.thbingen.de.eslmanager.network.SingletonCookieManager;
import app.syse.thbingen.de.eslmanager.scanner.SimpleScannerActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    SingletonCookieManager scm;
    OkHttpClient client;
    private String BASE_URL;

    String labelId;
    int eslId;

    @BindView(R.id.scan)
    Button btn_scan;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        scm = SingletonCookieManager.getInstance();
        c = this;

        TextView tv = findViewById(R.id.myText);

        SharedPreferences sharedpreferences = getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        BASE_URL = sharedpreferences.getString("BASE_URL", "192.168.178.178");

        tv.setText(scm.getCookieManager().getCookieStore().getCookies().size() + "");

        client = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(scm.getCookieManager()))
                .build();

        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(c, SimpleScannerActivity.class);
                startActivityForResult(i, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                progressBar.setVisibility(View.VISIBLE);
                labelId = data.getStringExtra("result");
                AT_ESL at_esl = new AT_ESL();
                at_esl.execute(buildRequestEsl());
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    private class AT_ESL extends AsyncTask<Request, Void, Void> {
        @Override
        protected Void doInBackground(Request... params) {
            sendRequest(params[0]);
            return null;
        }
    }

    private class AT_Product extends AsyncTask<Request, Void, Void> {
        @Override
        protected Void doInBackground(Request... params) {
            sendProductRequest(params[0]);
            return null;
        }
    }

    public Request buildRequestEsl(){
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host(BASE_URL)
                .port(8080)
                .addPathSegment("esls")
                .addPathSegment(labelId)
                .build();

        String url = httpUrl.toString();
        Log.d("URl", url);

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Log.d("Request", request.method());

        return request;
    }

    public Request buildRequestProduct(int productId){
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host(BASE_URL)
                .port(8080)
                .addPathSegment("products")
                .addPathSegment(productId+"")
                .build();

        String url = httpUrl.toString();
        Log.d("URl", url);

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Log.d("Request", request.method());

        return request;
    }

    private void sendRequest(final Request request) {
        final String errMessage = "Login fehlgeschlagen";
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("*** Failure ***", "Code: " + e.getMessage());
                //onLoginFailed();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseBody = response.body().string();
                Log.d("*** Response Code ***", "Code: " + response.code());
                Log.d("*** Response ***", responseBody);

                if (response.code() == 200 && responseBody.length() > 2){

                    ObjectMapper mapper = new ObjectMapper();
                    Esl e = mapper.readValue(responseBody.replace("[", "").replace("]", ""), Esl.class);
                    eslId = e.getId();

                    Log.d("Esl", e.toString());

                    if (e.getProductId() != -1){
                        AT_Product at_product = new AT_Product();
                        at_product.execute(buildRequestProduct(e.getProductId()));
                    } else {
                        Intent i = new Intent(c, EditLabelActiviy.class);
                        i.putExtra("eslId", eslId);
                        i.putExtra("labelId", labelId);
                        i.putExtra("productId", -1);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                        c.startActivity(i);
                    }
                } else {
                   showResponseError(response);
                }
            }
        });
    }

    public void sendProductRequest(Request request){
        final String errMessage = "Login fehlgeschlagen";
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("*** Failure ***", "Code: " + e.getMessage());
                //onLoginFailed();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseBody = response.body().string();
                Log.d("*** Response Code ***", "Code: " + response.code());
                Log.d("*** Response ***", responseBody);

                if (response.code() == 200 && responseBody.length() > 2){

                    ObjectMapper mapper = new ObjectMapper();
                    Product p = mapper.readValue(responseBody.replace("[", "").replace("]", ""), Product.class);

                    Log.d("Product", p.toString());
                    Intent i = new Intent(c, EditLabelActiviy.class);
                    i.putExtra("eslId", eslId);
                    i.putExtra("labelId", labelId);
                    i.putExtra("productId", p.getId());
                    i.putExtra("name", p.getName());
                    i.putExtra("price", p.getPrice());
                    i.putExtra("amount", p.getAmount());
                    i.putExtra("sku", p.getSku());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });

                    c.startActivity(i);

                } else {
                    showResponseError(response);
                }
            }
        });
    }

    public void showResponseError(final Response response){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                final AlertDialog alertDialog = new AlertDialog.Builder(c).create();
                alertDialog.setTitle("Response " + response.code());
                alertDialog.setMessage(response.message());
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                alertDialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });
    }
}

