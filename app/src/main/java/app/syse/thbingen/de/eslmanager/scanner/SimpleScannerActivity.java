package app.syse.thbingen.de.eslmanager.scanner;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Steffen on 25.12.2017.
 */

public class SimpleScannerActivity extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    Context c;
    String id;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this); // Programmatically initialize the scanner view
        setContentView(mScannerView); // Set the scanner view as the content view
        c = this;
    }
    @Override
    public void handleResult(Result rawResult) {
        String scanned = rawResult.getText();

        // Do something with the result here
        Log.v("scanner", scanned); // Prints scan results
        Log.v("scanner", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        if (scanned.length() == 18) {
            id = calculateHexLabelId(scanned);
            showDialog("Id kompatibel: " + id, true);
        } else {
            showDialog("Id nicht kompatibel: " + scanned, false);
        }


        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera(); // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCameraPreview(); //stopPreview
        mScannerView.stopCamera(); // Stop camera on pause
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mScannerView.stopCameraPreview(); //stopPreview
        mScannerView.stopCamera(); // Stop camera on pause
    }

    @Override
    public void onStop() {
        super.onStop();
        mScannerView.stopCameraPreview(); //stopPreview
        mScannerView.stopCamera(); // Stop camera on pause
    }

    public String calculateHexLabelId(String decId) {
        String shortedId = decId.substring(8);
        String hex = Integer.toString(new Integer(shortedId), 16);
        hex = insertPeriodically(hex.toUpperCase(), "-", 2);
        Log.d("Hex", hex);
        return hex;
    }

    public static String insertPeriodically(String text, String insert, int period)
    {
        StringBuilder builder = new StringBuilder(
                text.length() + insert.length() * (text.length()/period)+1);

        int index = 0;
        String prefix = "";
        while (index < text.length())
        {
            // Don't put the insert in the very first iteration.
            // This is easier than appending it *after* each substring
            builder.append(prefix);
            prefix = insert;
            builder.append(text.substring(index,
                    Math.min(index + period, text.length())));
            index += period;
        }
        return builder.toString();
    }

    public void showDialog(String message, boolean secondButton) {
        final AlertDialog alertDialog = new AlertDialog.Builder(c).create();
        alertDialog.setTitle("Code gefunden");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Neu scannen",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*dialog.dismiss();
                        alertDialog.dismiss();*/
                    }
                });

        if (secondButton) {
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Weiter",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            /*dialog.dismiss();
                            alertDialog.dismiss();*/
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("result",id);
                            setResult(Activity.RESULT_OK,returnIntent);
                            finish();
                        }
                    });
        }

        alertDialog.show();
    }
}
