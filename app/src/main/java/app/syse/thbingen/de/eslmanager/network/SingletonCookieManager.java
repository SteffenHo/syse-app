package app.syse.thbingen.de.eslmanager.network;

import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * Created by Steffen on 25.12.2017.
 */

public class SingletonCookieManager {
    private static volatile SingletonCookieManager sSoleInstance;

    CookieManager cookieManager;

    //private constructor.
    private SingletonCookieManager(){

        cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        //Prevent form the reflection api.
        if (sSoleInstance != null){
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static SingletonCookieManager getInstance() {
        if (sSoleInstance == null) { //if there is no instance available... create new one
            synchronized (SingletonCookieManager.class) {
                if (sSoleInstance == null) sSoleInstance = new SingletonCookieManager();
            }
        }

        return sSoleInstance;
    }

    //Make singleton from serialize and deserialize operation.
    protected SingletonCookieManager readResolve() {
        return getInstance();
    }

    public CookieManager getCookieManager() {
        return cookieManager;
    }

    public void setCookieManager(CookieManager cookieManager) {
        this.cookieManager = cookieManager;
    }
}
