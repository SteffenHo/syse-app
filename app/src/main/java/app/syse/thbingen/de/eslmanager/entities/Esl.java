package app.syse.thbingen.de.eslmanager.entities;

/**
 * Created by Steffen on 25.12.2017.
 */

public class Esl {

    private int id;
    int productId;
    String labelId;
    String status;
    String productString;

    /*public Esl(int id, int productId, String labelId, String status, String productString) {
        this.id = id;
        this.productId = productId;
        this.labelId = labelId;
        this.status = status;
        this.productString = productString;
    }*/

    public void setId(int id) {
        this.id = id;
    }

    public void setProductString(String productString) {
        this.productString = productString;
    }

    public int getId() {
        return id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductString() {
        return productString;
    }

    @Override
    public String toString() {
        return "Esl{" +
                "id=" + id +
                ", productId=" + productId +
                ", labelId='" + labelId + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
